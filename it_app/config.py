class Config(object):
	DEBUG = False

class DevelopmentConfig(Config):
	"""
	Development Configurations
	"""
	DEBUG = False
	SQLALCHEMY_ECHO = True

class ProductionConfig(Config):
	"""
	Production Config
	"""
	DEBUG = False

app_config = {
	'development': DevelopmentConfig,
	'production' : ProductionConfig
}

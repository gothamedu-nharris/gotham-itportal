from flask import render_template
from flask_login import login_required, current_user

from . import users
from .. import db

@users.route('/admin/homepage')
@login_required
def admin_homepage():
	return render_template('users/index.html', title="Home page")




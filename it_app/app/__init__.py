from flask import Flask
from config import app_config
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_login import LoginManager

login_manager = LoginManager()

db = SQLAlchemy()

def create_app(config_name):
	app = Flask(__name__, instance_relative_config=True)
	app.config.from_object(app_config[config_name])
	app.config.from_pyfile('config.py')
	# init db
	db.init_app(app)
	# login manager setup
	migrate = Migrate(app, db)
	from app import models
	login_manager.init_app(app)
	login_manager.login_message = "Unauthorized. This attempt will be logged"
	login_manager.login_view = "auth.login"
	Bootstrap(app)

	from .users import users as users_blueprint
	app.register_blueprint(users_blueprint)

	from .auth import auth as auth_blueprint
	app.register_blueprint(auth_blueprint)

	from .api import api as api_blueprint
	app.register_blueprint(api_blueprint, url_prefix="/api")

	from .logs import logs as logs_blueprint
	app.register_blueprint(logs_blueprint, url_prefix="/logs")

	return app

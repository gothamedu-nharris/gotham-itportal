from flask import flash, redirect, render_template, url_for
from flask_login import login_required, login_user, logout_user

from . import auth
from forms import LoginForm, RegistrationForm
from .. import db
from ..models import ITPerson
import string
import random

def generateStudentID(size=6, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

"""
Set this to public=FALSE
"""
@auth.route('/register', methods=['GET','POST'])
def register():
	form = RegistrationForm()
	if form.validate_on_submit():
		itperson = ITPerson(admin_email = form.email.data,
				admin_username = form.username.data,
				admin_fname = form.fname.data,
				admin_lname = form.lname.data,
				password = form.password.data,
				is_super_admin = False)
		db.session.add(itperson)
		db.session.commit()

		flash('Registration successful. You are currently on a limited user role')
		return redirect(url_for('auth.login'))
	return render_template('auth/register.html', form = form, title = 'Registration')

@auth.route('/login', methods=['GET','POST'])
def login():
	form = LoginForm()
	if form.validate_on_submit():
		itperson = ITPerson.query.filter_by(admin_email = form.email.data).first()
		if itperson is not None and itperson.verify_password(form.password.data):
			login_user(itperson)
			return redirect(url_for('users.admin_homepage'))
		else:
			flash('Wrong email or password combination')

	return render_template('auth/login.html', form = form, title='Login')

@auth.route('/logout')
@login_required
def logout():
	logout_user()
	flash('You are not logged out')
	return redirect(url_for('auth.login'))

from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo

from ..models import ITPerson

class RegistrationForm(FlaskForm):
	email = StringField('Email', validators = [DataRequired(), Email()])
	username = StringField('Username', validators = [DataRequired()])
	fname = StringField('First name', validators = [DataRequired()])
	lname = StringField('Last name', validators = [DataRequired()])
	password = PasswordField('Password', validators = [DataRequired(),
							EqualTo('confirm_password')])
	confirm_password = PasswordField('Confirm Password')
	submit = SubmitField('Register')

	def valid_email(self, field):
		if ITPerson.query.filter_by(student_email=field.data).first():
			raise ValidationError('Email already registered')
	def valid_username(self, field):
		if ITPerson.query.filter_by(student_username = field.data).first():
			raise ValidationError('Username already in use')

class LoginForm(FlaskForm):
	email = StringField('Email', validators = [DataRequired(), Email()])
	password = PasswordField('Password', validators = [DataRequired()])
	submit = SubmitField('Login')

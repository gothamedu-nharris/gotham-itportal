from flask import render_template, jsonify, flash, redirect
from flask_login import login_required, current_user
from flask_cors import cross_origin
import json

from . import api
from .. import db
from ..models import ITPerson, EnvVariables, Logs
from helpers import getDownloadURL

@api.route('/me', methods=['GET'])
@login_required
@cross_origin(supports_credentials=True)
def return_current_user():
	curr_user = {"name":'{0} {1}'.format(current_user.admin_fname, current_user.admin_lname),
		"email":current_user.admin_email, "username":current_user.admin_username,
		"is_super_admin":current_user.is_super_admin}
	return jsonify(curr_user)

@api.route('/cloud_keys', methods=['GET'])
@login_required
@cross_origin(supports_credentials=True)
def get_all_variables():
	all_vars = EnvVariables.query.all()
	variables = {"total":0}
	if len(all_vars) > 0:
		variables['total'].update(len(all_vars))
		variables.update({"vars":{}})
		for var in all_vars:
			variables['vars'].update({"slack_token":var.slack_alert, "email_tokens":var.email_tokens})
	return jsonify(variables)

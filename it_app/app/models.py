from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login_manager

#admins class
class ITPerson(UserMixin, db.Model):
	__tablename__ = 'admins'
	id = db.Column(db.Integer, primary_key=True)
	admin_email = db.Column(db.String(100), index=True, unique=True)
	admin_username = db.Column(db.String(100), index=True, unique=True)
	admin_fname = db.Column(db.String(100), index=True)
	admin_lname = db.Column(db.String(100), index=True)
	pass_hash = db.Column(db.String(300), index=True)
	is_super_admin = db.Column(db.Boolean, default=False)

	@property
	def password(self):
		raise AttributeError('Password is not readable')

	@password.setter
	def password(self, password):
		self.pass_hash = generate_password_hash(password)

	def verify_password(self, password):
		return check_password_hash(self.pass_hash, password)

	def __repr__(self):
		return '<ITPerson: {}>'.format(self.admin_username)

@login_manager.user_loader
def load_user(user_id):
	return ITPerson.query.get(int(user_id))

#User logs will be here
class Logs(db.Model):
	__tablename__ = 'logs'

	id = db.Column(db.Integer, primary_key=True)
	log_time = db.Column(db.String(100), index=True)
	log_data = db.Column(db.String(300), index=True)
	log_type = db.Column(db.String(100), index=True)
	log_user = db.Column(db.String(100), index=True)

	def __repr__(self):
		return '<Logs: {}>'.format(self.id)

#Encrypted Slack tokens will be here
class EnvVariables(db.Model):
	__tablename__ = 'recentregister'

	id = db.Column(db.Integer, primary_key=True)
	slack_alert = db.Column(db.String(100), index=True)
	email_tokens = db.Column(db.String(200), index=True)

	def __repr__(self):
		return '<RecentRegister: {}>'.format(self.student_email)
